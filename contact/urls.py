from django.urls import path 
from . import views 

urlpatterns = [
path('',views.index, name='index'),
path('ajouter',views.ajouter, name='ajouter'),
path('afficher',views.afficher, name='afficher'),
path('chercher',views.chercher, name='chercher'),
path('supprimer',views.supprimer, name='supprimer'),
path('modifier',views.modifier, name='modifier'),



]