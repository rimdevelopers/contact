from django import forms



class ContactForm(forms.Form):
	pseudo = forms.CharField(required=True,max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	nom = forms.CharField(required=True , max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	prenom = forms.CharField(required=True,max_length=20,  widget=forms.TextInput(attrs={'class': 'form-control'} ))
	email = forms.EmailField(required=True,max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'} ))
	tel = forms.CharField(required=True,max_length=8, widget=forms.TextInput(attrs={'class': 'form-control'} ))

	
class ContaCher(forms.Form):
        pseudo = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'} ))
        