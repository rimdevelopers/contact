# Generated by Django 2.0.2 on 2018-02-28 01:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0002_auto_20180226_2253'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='mes_contact',
            new_name='contacts',
        ),
    ]
