from django.shortcuts import render
from contact import models
from contact import  forms
from .models import contacts





# Create your views here.
def index(request):
	

	return render(request,'contact/index.html')


def ajouter(request):

  form_data=forms.ContactForm(request.POST or None)
  msg=''
  
  if form_data.is_valid():
       contacts=models.contacts()
       contacts.pseudo=form_data.cleaned_data['pseudo']
       contacts.nom=form_data.cleaned_data['nom']
       contacts.prenom=form_data.cleaned_data['prenom']
       contacts.email=form_data.cleaned_data['email']
       contacts.tel=form_data.cleaned_data['tel']

       
       contacts.save()
       msg='data is saved'


  context={
        'formregister':form_data,
         'msg':msg
      
    }
  return render(request,'contact/ajouter.html',context)

def afficher(request):
	contacts=models.contacts.objects.all()
	donnees={
        'contacts':contacts
         
      
    }

    
	

	return render(request,'contact/afficher.html',donnees)



def chercher(request):


        msg="ce contact n'exist pas"
        form_data=forms.ContaCher(request.POST or None)
        form1={
                'formregister':form_data,
                'msg':msg
                }
        form ={'formregister':form_data}
        mes_contacts=models.contacts.objects.all()

        
        if form_data.is_valid():
                mes_contact=models.contacts.objects.all()

                pseudo=form_data.cleaned_data['pseudo']
                j=[]
                for cont in mes_contact:
                  j.append(cont.pseudo)
                
                if pseudo in j:
                        i = contacts.objects.get(pseudo=pseudo)

                        context ={
                        'contact':i}
                        return render(request,'contact/cher_contact.html',context)
                else:
                        return render(request,'contact/chercher.html',form1)
        return render(request,'contact/chercher.html',form)
def supprimer(request):
        msg="ce contact n'exist pas"
        form_data=forms.ContaCher(request.POST or None)
        form1={
                'formregister':form_data,
                'msg':msg
                }
        form ={'formregister':form_data} 
              
        if form_data.is_valid():
                mes_contact=models.contacts.objects.all()
                pseudo=form_data.cleaned_data['pseudo']
                j=[]
                for cont in mes_contact:
                  j.append(cont.pseudo)
              
                if pseudo in j:
                        i=contacts.objects.get(pseudo=pseudo)
                        

                        i.delete()
                        return render(request,'contact/endsup.html')
                else:
                        return render(request,'contact/supprimer.html',form1)
        return render(request,'contact/supprimer.html',form)

def modifier(request):
        msg="ce contact n'exist pas"
        form_data1=forms.ContactForm(request.POST or None)
        form_data=forms.ContaCher(request.POST or None)
        form1 ={'formregister':form_data,'msg':msg}
        form ={'formregister':form_data} 
        if form_data.is_valid():
                mes_contact=models.contacts.objects.all()
                pseudo=form_data.cleaned_data['pseudo']

                j=[]
                for cont in mes_contact:
                  j.append(cont.pseudo)
                if pseudo in j:
                        i = contacts.objects.get(pseudo=pseudo)
                        if form_data1.is_valid():
                                i.pseudo=form_data1.cleaned_data['pseudo']
                                i.nom=form_data1.cleaned_data['nom']
                                i.prenom=form_data1.cleaned_data['prenom']
                                i.email=form_data1.cleaned_data['email']
                                i.tel=form_data1.cleaned_data['tel']
                                i.save()
                        msg1='le Contact a éte modifié'
                        form2 = {'formregister':form_data1,'msg':msg1,'pseudo':i.pseudo,'nom':i.nom,'prenom':i.prenom,'email':i.email,'tel':i.tel }
                        return render(request,'contact/modifier1.html',form2)
                                
                                
                else:
                        return render(request,'contact/modifier.html',form1)
        return render(request,'contact/modifier.html',form)
